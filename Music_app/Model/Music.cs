﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Music_app.Model
{
    public class Music
    {
        public string SongName { get; set; }
        public string SongPath { get; set; }
        public string TotalTime { get; set; }
        public string Singer { get; set; }
        public Bitmap Imgsrc { get; set; }
        public TimeSpan Time { get; set; }

        //default contructor
        public Music()
        {

        }

        public Music(string SongName, string SongPath,string TotalTime, string Singer, Bitmap Imgsrc, TimeSpan Time)
        {
            this.SongName = SongName;
            this.SongPath = SongPath;
            this.TotalTime = TotalTime;
            this.Singer = Singer;
            this.Imgsrc = Imgsrc;
            this.Time = Time;
        }
    }
}
