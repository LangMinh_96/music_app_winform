﻿namespace Music_app.UC.Control
{
    partial class SongItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbsongName = new System.Windows.Forms.Label();
            this.lbSinger = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.ptbImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ptbImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lbsongName
            // 
            this.lbsongName.AutoSize = true;
            this.lbsongName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbsongName.ForeColor = System.Drawing.Color.White;
            this.lbsongName.Location = new System.Drawing.Point(57, 11);
            this.lbsongName.MinimumSize = new System.Drawing.Size(250, 35);
            this.lbsongName.Name = "lbsongName";
            this.lbsongName.Size = new System.Drawing.Size(250, 35);
            this.lbsongName.TabIndex = 1;
            this.lbsongName.Text = "Ừ có anh đây";
            this.lbsongName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbsongName.Click += new System.EventHandler(this.lbsongName_Click);
            // 
            // lbSinger
            // 
            this.lbSinger.AutoSize = true;
            this.lbSinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSinger.ForeColor = System.Drawing.Color.White;
            this.lbSinger.Location = new System.Drawing.Point(380, 11);
            this.lbSinger.MinimumSize = new System.Drawing.Size(75, 35);
            this.lbSinger.Name = "lbSinger";
            this.lbSinger.Size = new System.Drawing.Size(75, 35);
            this.lbSinger.TabIndex = 2;
            this.lbSinger.Text = "Tino";
            this.lbSinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbSinger.Click += new System.EventHandler(this.lbsongName_Click);
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTime.ForeColor = System.Drawing.Color.White;
            this.lbTime.Location = new System.Drawing.Point(479, 11);
            this.lbTime.MinimumSize = new System.Drawing.Size(0, 35);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(39, 35);
            this.lbTime.TabIndex = 2;
            this.lbTime.Text = "06:09";
            this.lbTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbTime.Click += new System.EventHandler(this.lbsongName_Click);
            // 
            // ptbImage
            // 
            this.ptbImage.Location = new System.Drawing.Point(5, 5);
            this.ptbImage.Name = "ptbImage";
            this.ptbImage.Size = new System.Drawing.Size(45, 46);
            this.ptbImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptbImage.TabIndex = 0;
            this.ptbImage.TabStop = false;
            this.ptbImage.Click += new System.EventHandler(this.lbsongName_Click);
            // 
            // SongItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Goldenrod;
            this.Controls.Add(this.lbTime);
            this.Controls.Add(this.lbSinger);
            this.Controls.Add(this.lbsongName);
            this.Controls.Add(this.ptbImage);
            this.Name = "SongItem";
            this.Size = new System.Drawing.Size(555, 55);
            ((System.ComponentModel.ISupportInitialize)(this.ptbImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ptbImage;
        private System.Windows.Forms.Label lbsongName;
        private System.Windows.Forms.Label lbSinger;
        private System.Windows.Forms.Label lbTime;
    }
}
