﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Music_app.Model;

namespace Music_app.UC.Control
{
    public partial class SongItem : UserControl
    {
        private Music music;

        //delegate and event that this item is clicked
        public delegate void SongClicked(Music music);
        public event SongClicked songItemClick;

        //Default contructor
        public SongItem()
        {
            InitializeComponent();
        }

        public SongItem(Music music)
        {
            InitializeComponent();
            this.music = music;

            ptbImage.Image = music.Imgsrc;
            lbsongName.Text = music.SongName;
            lbSinger.Text = music.Singer;
            lbTime.Text = music.TotalTime;

        }

        //return the music object of this song item
        public Music GetMusic()
        {
            return this.music;
        }

        //When the song item click hanlder the event the MyMusic
        private void lbsongName_Click(object sender, EventArgs e)
        {
            if(songItemClick != null)
            {
                songItemClick(this.music);
            }
        }
    }
}
