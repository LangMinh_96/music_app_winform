﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Music_app.Model;
using Music_app.UC.Control;
using System.IO;

namespace Music_app.UC
{
    public partial class MyMusic : UserControl
    {
        private List<Music> list = new List<Music>();

        //When a Song item is click we send an event to main form and main form will play that song item
        public delegate void MusicClick(Music music);
        public event MusicClick click_event;

        //Create delegate and event when asyn the music in folder is finished
        public delegate void GetMusicOk(List<Music> musics);
        public event GetMusicOk getOk;

        public delegate void GetMusic(List<Music> musics);
        public event GetMusicOk get;

        //Contructor default
        public MyMusic()
        {
            InitializeComponent();
            //Set auto scroll item in the flow panel
            song_panel.AutoScroll = true;
            //disable horizontal Scroll in the flow panel
            song_panel.HorizontalScroll.Enabled = false;
        }

        public MyMusic(List<Music> list)
        {
            InitializeComponent();
            this.list = list;
            if(get != null)
            {
                get(this.list);
            }
            song_panel.AutoScroll = true;
        }

        //When this user control load event
        private void MyMusic_Load(object sender, EventArgs e)
        {
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    SongItem song = new SongItem(item);
                    //Set click on song item in the layout
                    song.Click += Song_Click;
                    song.songItemClick += Song_songItemClick;
                    song_panel.Controls.Add(song);
                }
            }
            else
            {
                //Open folder dialog to select path
                FolderBrowserDialog folder = new FolderBrowserDialog();
                folder.Description = "hãy chọn thư mục chứa nhạc của bạn";
                if (folder.ShowDialog() == DialogResult.OK)
                {
                    list = GetMusics(folder.SelectedPath);
                    if(getOk != null)
                    {
                        getOk(list);
                    }
                    foreach (var item in list)
                    {
                        SongItem song = new SongItem(item);
                        //Set click on song item in the layout
                        song.Click += Song_Click;
                        song.songItemClick += Song_songItemClick;
                        song_panel.Controls.Add(song);
                    }
                }
            }
        }

        //When song item is click
        private void Song_songItemClick(Music music)
        {
            if (click_event != null)
            {
                click_event(music);
            }
        }

        //When song item is click
        private void Song_Click(object sender, EventArgs e)
        {
            //Day delegate sang main cho main chay nhac
            if (click_event != null)
            {
                var result = sender as SongItem;
                click_event(result.GetMusic());
            }
        }

        //return list music of this user control
        public List<Music> GetMusics()
        {
            return this.list;
        }

        //Get music in the folder selected
        private List<Music> GetMusics(string path)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(path);//Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.mp3"); //Getting Text files
                List<Music> result = new List<Music>();

                foreach (FileInfo file in Files)
                {
                    Music music = new Music();
                    TagLib.File tagFile = TagLib.File.Create(file.FullName);

                    if (tagFile.Tag.Title != null)
                    {
                        music.SongName = tagFile.Tag.Title;
                    }
                    else
                    {
                        music.SongName = file.Name;
                    }
                    int hours = tagFile.Properties.Duration.Hours;
                    int min = tagFile.Properties.Duration.Minutes;
                    int sec = tagFile.Properties.Duration.Seconds;

                    music.Time = tagFile.Properties.Duration;
                    music.TotalTime = ((hours < 10 ? "0" + hours.ToString() : hours.ToString()) + ":")
                        + (min < 10 ? "0" + min.ToString() : min.ToString()) + ":" +
                        (sec < 10 ? "0" + sec.ToString() : sec.ToString());

                    if (tagFile.Tag.FirstPerformer != null)
                    {
                        music.Singer = tagFile.Tag.FirstPerformer;
                    }
                    else
                    {
                        music.Singer = "Unknow";
                    }
                    music.SongPath = file.FullName;

                    var mStream = new MemoryStream();
                    var firstPicture = tagFile.Tag.Pictures.FirstOrDefault();
                    if (firstPicture != null)
                    {
                        byte[] pData = firstPicture.Data.Data;
                        mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                        var bm = new Bitmap(mStream, false);
                        mStream.Dispose();
                        music.Imgsrc = bm;
                    }
                    else
                    {
                        music.Imgsrc = Music_app.Properties.Resources.photo;
                    }
                    result.Add(music);
                }
                return result;
            }
            catch
            {
                return null;
            }

        }

        //When seach textbox type and press enter
        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                song_panel.Controls.Clear();
                //if text search is not emty we search in the list
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {

                    var restult = list.Where(x => x.SongName.Contains(txtSearch.Text)).ToList();
                    foreach (var item in restult)
                    {
                        SongItem song = new SongItem(item);
                        song.Click += Song_Click;
                        song.songItemClick += Song_songItemClick;
                        song_panel.Controls.Add(song);
                    }
                }
                //If search textbox emty we return a current list(have all file mp3 in there)
                else
                {
                    foreach (var item in list)
                    {
                        SongItem song = new SongItem(item);
                        song.Click += Song_Click;
                        song.songItemClick += Song_songItemClick;
                        song_panel.Controls.Add(song);
                    }
                }
            }
        }
    }
}
