﻿using Music_app.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using WMPLib;
using TagLib;
using Music_app.UC.Control;
using Music_app.UC;

namespace Music_app
{
    public partial class Main_Form : Form
    {
        private WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        private MyMusic music = null;
        private bool isRepeat = false;
        private bool isMute = false;
        private bool isShuff = false;
        private Music currentFile = null;
        private int stage = (int)MenuStage.MyMusic;
        private MyMusic current = null;

        private enum MenuStage
        {
            MyMusic,
            RecentPlay,
            NowPlaying,
            Playlist,
            Setting
        }

        int sec = 0, min = 0, hour = 0, total;

        private List<Music> list = new List<Music>();
        private List<Music> RecentPlaysList = new List<Music>();

        public delegate void AsynOk(List<Music> musics);
        public event AsynOk ok;

        public Main_Form()
        {
            InitializeComponent();

            player.settings.volume = slVolume.Value;

            player.PlayStateChange += Player_PlayStateChange;
            timer1.Tick += Timer1_Tick;
        }

        //when a file mp3 change the stage is_playing or stoped
        private void Player_PlayStateChange(int NewState)
        {
            if (NewState == (int)WMPPlayState.wmppsStopped)
            {
                sec = 0;
                min = 0;
                hour = 0;
                total = 0;
                timeSlide.Value = total;
                if (isRepeat)
                {
                    player.controls.play();
                    timer1.Start();
                }
                else
                {
                    //next
                    var song = Next(currentFile);
                    PlayFile(song);
                }
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_mini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            lbLogo.Visible = true;
        }

        //Button Menu click 
        private void bunifuImageButton4_Click(object sender, EventArgs e)
        {
            if (menu_panel.Width >= 200)
            {
                menu_panel.Width = 40;
                lbLogo.Visible = false;
                panel_Control.Width = panel_Control.Width + (200 - 40);
                btnMenu.Location = new Point(7, 25);
            }
            else if (menu_panel.Width <= 40)
            {
                menu_panel.Width = 200;
                panel_Control.Width = panel_Control.Width - (200 - 40);
                lbLogo.Visible = true;
                btnMenu.Location = new Point(165, 6);
            }
        }

        //Volume change value
        private void slVolume_ValueChanged(object sender, EventArgs e)
        {
            if (slVolume.Value == 0)
            {
                btnVolume.Image = Music_app.Properties.Resources.Mute_32px;
            }
            else if (slVolume.Value <= 65)
            {
                btnVolume.Image = Music_app.Properties.Resources.Speaker_32px;
            }
            else
            {
                btnVolume.Image = Music_app.Properties.Resources.Audio_32px;
            }
            player.settings.volume = slVolume.Value;
        }

        //Button My Music click
        private void btn_MyMusic_Click(object sender, EventArgs e)
        {
            music = new MyMusic();
            stage = (int)MenuStage.MyMusic;
            music.click_event += Music_click_event;
            music.getOk += Music_getOk;
            music.Dock = DockStyle.Fill;
            main_panel.Controls.Add(music);
            btn_MyMusic.selected = true;
            btnRecentPlay.selected = false;
            btnNowPlaying.selected = false;
            btnPlaylist.selected = false;
            btnSetting.selected = false;
        }

        //When app is asyn file mp3 ok
        private void Music_getOk(List<Music> musics)
        {
            this.list = musics;
            if(ok != null)
            {
                ok(this.list);
            }
        }

        //When a mp3 file click and then play it
        private void Music_click_event(Music music)
        {
            if (player.playState == WMPPlayState.wmppsPlaying)
            {
                PlayFile(music);
            }
            else
            {
                PlayFile(music);
            }
        }

        //Timer tick to change current time and time slide
        private void Timer1_Tick(object sender, EventArgs e)
        {
            sec++;

            total++;

            timeSlide.Value = total;

            if (sec > 59)
            {
                sec = 0;
                min++;
            }
            if (min > 59)
            {
                hour++;
                min = 0;
            }
            currentTime.Text = ((hour < 10 ? "0" + hour.ToString() : hour.ToString()) + ":") + (min < 10 ? "0" + min.ToString() : min.ToString()) + ":" + (sec < 10 ? "0" + sec.ToString() : sec.ToString());
        }

        //When volume click is mute or not
        private void btnVolume_Click(object sender, EventArgs e)
        {
            isMute = !isMute;
            if (isMute)
            {
                btnVolume.Image = Music_app.Properties.Resources.Mute_32px;
                player.settings.volume = 0;
            }
            else
            {
                if (slVolume.Value == 0)
                {
                    btnVolume.Image = Music_app.Properties.Resources.Mute_32px;
                }
                else if (slVolume.Value <= 65)
                {
                    btnVolume.Image = Music_app.Properties.Resources.Speaker_32px;
                }
                else
                {
                    btnVolume.Image = Music_app.Properties.Resources.Audio_32px;
                }
                player.settings.volume = slVolume.Value;
            }
        }

        //button next click event
        private void btnNext_Click(object sender, EventArgs e)
        {
            var song = Next(currentFile);
            PlayFile(song);
        }

        //Play a file mp3
        private void PlayFile(Music music)
        {
            timer1.Stop();
            player.URL = music.SongPath;
            currentFile = music;

            songTitle.Text = music.SongName;
            totalTime.Text = music.TotalTime;
            timeSlide.MaximumValue = (int)music.Time.TotalSeconds;

            sec = 0;
            min = 0;
            hour = 0;
            total = 0;
            timeSlide.Value = total;

            btnPlay.Image = Music_app.Properties.Resources.Pause_32px;
            var result = RecentPlaysList.Exists(x => x.SongName.Equals(music.SongName) && x.SongPath.Equals(music.SongPath));
            if (!result)
            {
                RecentPlaysList.Add(music);
            }

            player.controls.play();
            timer1.Start();
        }

        //button shuff click event
        private void btnShuff_Click(object sender, EventArgs e)
        {
            isShuff = !isShuff;
            if (isShuff)
            {
                PlayFile(currentFile);
                btnShuff.BackColor = Color.FromArgb(192, 255, 255);
            }
            else
            {
                btnShuff.BackColor = Color.FromArgb(37, 47, 60);
            }
        }

        //Prev button click event
        private void btnPrev_Click(object sender, EventArgs e)
        {
            var song = Prev(currentFile);
            PlayFile(song);
        }

        //Play or pause music
        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (player.playState == WMPPlayState.wmppsPlaying)
            {
                player.controls.pause();
                timer1.Stop();
                btnPlay.Image = Music_app.Properties.Resources.Play_32px;
            }
            else if (player.playState == WMPPlayState.wmppsPaused)
            {
                player.controls.play();
                timer1.Start();
                btnPlay.Image = Music_app.Properties.Resources.Pause_32px;
            }
        }

        private void btnRecentPlay_Click(object sender, EventArgs e)
        {
            if (music != null)
            {
                music.Dispose();
            }

            main_panel.Controls.Clear();
            music = new MyMusic(RecentPlaysList);
            music.Dock = DockStyle.Fill;
            stage = (int)MenuStage.RecentPlay;

            music.click_event += Music_click_event;
            music.get += Music_get;

            main_panel.Controls.Add(music);
            btn_MyMusic.selected = false;
            btnRecentPlay.selected = true;
            btnNowPlaying.selected = false;
            btnPlaylist.selected = false;
            btnSetting.selected = false;
        }

        private void Music_get(List<Music> musics)
        {
            
        }

        private void btnNowPlaying_Click(object sender, EventArgs e)
        {
            if (music != null)
            {
                music.Dispose();
            }

            main_panel.Controls.Clear();
            music = new MyMusic(list);

            stage = (int)MenuStage.NowPlaying;
            music.Dock = DockStyle.Fill;
            music.click_event += Music_click_event;
            music.get += Music_get;

            main_panel.Controls.Add(music);
            btn_MyMusic.selected = false;
            btnRecentPlay.selected = false;
            btnNowPlaying.selected = true;
            btnPlaylist.selected = false;
            btnSetting.selected = false;
        }

        //When click app will repeat that song or not
        private void btnRepeat_Click(object sender, EventArgs e)
        {
            isRepeat = !isRepeat;
            if (isRepeat)
            {
                btnRepeat.BackColor = Color.FromArgb(192, 255, 255);
            }
            else
            {
                btnRepeat.BackColor = Color.FromArgb(37, 47, 60);
            }
        }

        //prev file mp3
        private Music Prev(Music currentFile)
        {
            Music music = currentFile;
            List<Music> temp = null;

            if (stage == (int)MenuStage.RecentPlay)
            {
                temp = RecentPlaysList;
            }
            else if (stage == (int)MenuStage.MyMusic || stage == (int)MenuStage.NowPlaying)
            {
                temp = list;
            }

            if (isShuff)
            {
                //Shuff file in list
                Random random = new Random();
                int vt = random.Next(0, temp.Count);
                music = temp[vt];
            }
            else
            {
                if (currentFile != null)
                {
                    int vt = temp.IndexOf(currentFile);

                    int lastFile = temp.Count - 1;
                    if ((vt - 1) < 0)
                    {
                        music = temp[lastFile];
                    }
                    else
                    {
                        music = temp[vt - 1];
                    }
                }
                else
                {
                    music = temp[0];
                }
            }
            return music;
        }

        //next file mp3
        private Music Next(Music currentFile)
        {
            Music music = currentFile;
            List<Music> temp = null;

            if (stage == (int)MenuStage.RecentPlay)
            {
                temp = RecentPlaysList;
            }
            else if (stage == (int)MenuStage.MyMusic || stage == (int)MenuStage.NowPlaying)
            {
                temp = list;
            }
            if (isShuff)
            {
                //Shuff file in list
                Random random = new Random();
                int vt = random.Next(0, temp.Count);
                music = temp[vt];
            }
            else
            {
                if (currentFile != null)
                {
                    int vt = temp.IndexOf(currentFile);

                    int lastFile = temp.Count - 1;
                    if ((vt + 1) <= lastFile)
                    {
                        music = temp[vt + 1];
                    }
                    else if ((vt + 1) > lastFile)
                    {
                        music = temp[0];
                    }
                }
                else
                {
                    music = temp[0];
                }
            }
            return music;
        }
    }
}