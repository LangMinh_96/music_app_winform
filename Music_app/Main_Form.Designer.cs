﻿namespace Music_app
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.top_panel = new System.Windows.Forms.Panel();
            this.songTitle = new System.Windows.Forms.Label();
            this.btn_mini = new Bunifu.Framework.UI.BunifuImageButton();
            this.btn_Close = new Bunifu.Framework.UI.BunifuImageButton();
            this.menu_panel = new System.Windows.Forms.Panel();
            this.btnSetting = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnPlaylist = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnNowPlaying = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRecentPlay = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_MyMusic = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbLogo = new System.Windows.Forms.Label();
            this.btnMenu = new Bunifu.Framework.UI.BunifuImageButton();
            this.panel_Control = new System.Windows.Forms.Panel();
            this.main_panel = new System.Windows.Forms.Panel();
            this.Music_control = new System.Windows.Forms.Panel();
            this.totalTime = new System.Windows.Forms.Label();
            this.currentTime = new System.Windows.Forms.Label();
            this.timeSlide = new Bunifu.Framework.UI.BunifuSlider();
            this.slVolume = new Bunifu.Framework.UI.BunifuSlider();
            this.btnNext = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnVolume = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnShuff = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRepeat = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnPrev = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnPlay = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bunifuDragControl2 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bunifuElipse2 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuElipse3 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.top_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_mini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).BeginInit();
            this.menu_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).BeginInit();
            this.panel_Control.SuspendLayout();
            this.Music_control.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnShuff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRepeat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlay)).BeginInit();
            this.SuspendLayout();
            // 
            // top_panel
            // 
            this.top_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.top_panel.Controls.Add(this.songTitle);
            this.top_panel.Controls.Add(this.btn_mini);
            this.top_panel.Controls.Add(this.btn_Close);
            this.top_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.top_panel.Location = new System.Drawing.Point(0, 0);
            this.top_panel.Name = "top_panel";
            this.top_panel.Size = new System.Drawing.Size(800, 41);
            this.top_panel.TabIndex = 0;
            // 
            // songTitle
            // 
            this.songTitle.AutoSize = true;
            this.songTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.songTitle.ForeColor = System.Drawing.Color.White;
            this.songTitle.Location = new System.Drawing.Point(77, 9);
            this.songTitle.MinimumSize = new System.Drawing.Size(650, 0);
            this.songTitle.Name = "songTitle";
            this.songTitle.Size = new System.Drawing.Size(650, 16);
            this.songTitle.TabIndex = 1;
            this.songTitle.Text = "Ừ Có anh đây";
            this.songTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_mini
            // 
            this.btn_mini.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.btn_mini.Image = global::Music_app.Properties.Resources.Subtract_48px;
            this.btn_mini.ImageActive = null;
            this.btn_mini.Location = new System.Drawing.Point(733, 3);
            this.btn_mini.Name = "btn_mini";
            this.btn_mini.Size = new System.Drawing.Size(29, 26);
            this.btn_mini.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_mini.TabIndex = 0;
            this.btn_mini.TabStop = false;
            this.btn_mini.Zoom = 10;
            this.btn_mini.Click += new System.EventHandler(this.btn_mini_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(79)))), ((int)(((byte)(125)))));
            this.btn_Close.Image = global::Music_app.Properties.Resources.Delete_48px;
            this.btn_Close.ImageActive = null;
            this.btn_Close.Location = new System.Drawing.Point(768, 3);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(29, 26);
            this.btn_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btn_Close.TabIndex = 0;
            this.btn_Close.TabStop = false;
            this.btn_Close.Zoom = 10;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // menu_panel
            // 
            this.menu_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.menu_panel.Controls.Add(this.btnSetting);
            this.menu_panel.Controls.Add(this.btnPlaylist);
            this.menu_panel.Controls.Add(this.btnNowPlaying);
            this.menu_panel.Controls.Add(this.btnRecentPlay);
            this.menu_panel.Controls.Add(this.btn_MyMusic);
            this.menu_panel.Controls.Add(this.panel1);
            this.menu_panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.menu_panel.Location = new System.Drawing.Point(0, 41);
            this.menu_panel.Name = "menu_panel";
            this.menu_panel.Size = new System.Drawing.Size(203, 445);
            this.menu_panel.TabIndex = 1;
            // 
            // btnSetting
            // 
            this.btnSetting.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSetting.BorderRadius = 0;
            this.btnSetting.ButtonText = "Setting";
            this.btnSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetting.DisabledColor = System.Drawing.Color.Gray;
            this.btnSetting.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSetting.Iconimage = global::Music_app.Properties.Resources.Settings_32px;
            this.btnSetting.Iconimage_right = null;
            this.btnSetting.Iconimage_right_Selected = null;
            this.btnSetting.Iconimage_Selected = null;
            this.btnSetting.IconMarginLeft = -5;
            this.btnSetting.IconMarginRight = 0;
            this.btnSetting.IconRightVisible = true;
            this.btnSetting.IconRightZoom = 0D;
            this.btnSetting.IconVisible = true;
            this.btnSetting.IconZoom = 55D;
            this.btnSetting.IsTab = false;
            this.btnSetting.Location = new System.Drawing.Point(3, 303);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnSetting.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnSetting.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSetting.selected = false;
            this.btnSetting.Size = new System.Drawing.Size(194, 48);
            this.btnSetting.TabIndex = 1;
            this.btnSetting.Text = "Setting";
            this.btnSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetting.Textcolor = System.Drawing.Color.White;
            this.btnSetting.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnPlaylist
            // 
            this.btnPlaylist.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnPlaylist.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnPlaylist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlaylist.BorderRadius = 0;
            this.btnPlaylist.ButtonText = "Playlist";
            this.btnPlaylist.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlaylist.DisabledColor = System.Drawing.Color.Gray;
            this.btnPlaylist.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPlaylist.Iconimage = global::Music_app.Properties.Resources.Playlist_32px;
            this.btnPlaylist.Iconimage_right = null;
            this.btnPlaylist.Iconimage_right_Selected = null;
            this.btnPlaylist.Iconimage_Selected = null;
            this.btnPlaylist.IconMarginLeft = -5;
            this.btnPlaylist.IconMarginRight = 0;
            this.btnPlaylist.IconRightVisible = true;
            this.btnPlaylist.IconRightZoom = 0D;
            this.btnPlaylist.IconVisible = true;
            this.btnPlaylist.IconZoom = 55D;
            this.btnPlaylist.IsTab = false;
            this.btnPlaylist.Location = new System.Drawing.Point(3, 249);
            this.btnPlaylist.Name = "btnPlaylist";
            this.btnPlaylist.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnPlaylist.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnPlaylist.OnHoverTextColor = System.Drawing.Color.White;
            this.btnPlaylist.selected = false;
            this.btnPlaylist.Size = new System.Drawing.Size(194, 48);
            this.btnPlaylist.TabIndex = 1;
            this.btnPlaylist.Text = "Playlist";
            this.btnPlaylist.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPlaylist.Textcolor = System.Drawing.Color.White;
            this.btnPlaylist.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnNowPlaying
            // 
            this.btnNowPlaying.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnNowPlaying.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnNowPlaying.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNowPlaying.BorderRadius = 0;
            this.btnNowPlaying.ButtonText = "Now Playing";
            this.btnNowPlaying.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNowPlaying.DisabledColor = System.Drawing.Color.Gray;
            this.btnNowPlaying.Iconcolor = System.Drawing.Color.Transparent;
            this.btnNowPlaying.Iconimage = global::Music_app.Properties.Resources.Audio_Wave_32px;
            this.btnNowPlaying.Iconimage_right = null;
            this.btnNowPlaying.Iconimage_right_Selected = null;
            this.btnNowPlaying.Iconimage_Selected = null;
            this.btnNowPlaying.IconMarginLeft = -5;
            this.btnNowPlaying.IconMarginRight = 0;
            this.btnNowPlaying.IconRightVisible = true;
            this.btnNowPlaying.IconRightZoom = 0D;
            this.btnNowPlaying.IconVisible = true;
            this.btnNowPlaying.IconZoom = 55D;
            this.btnNowPlaying.IsTab = false;
            this.btnNowPlaying.Location = new System.Drawing.Point(3, 195);
            this.btnNowPlaying.Name = "btnNowPlaying";
            this.btnNowPlaying.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnNowPlaying.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnNowPlaying.OnHoverTextColor = System.Drawing.Color.White;
            this.btnNowPlaying.selected = false;
            this.btnNowPlaying.Size = new System.Drawing.Size(194, 48);
            this.btnNowPlaying.TabIndex = 1;
            this.btnNowPlaying.Text = "Now Playing";
            this.btnNowPlaying.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNowPlaying.Textcolor = System.Drawing.Color.White;
            this.btnNowPlaying.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNowPlaying.Click += new System.EventHandler(this.btnNowPlaying_Click);
            // 
            // btnRecentPlay
            // 
            this.btnRecentPlay.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnRecentPlay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnRecentPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRecentPlay.BorderRadius = 0;
            this.btnRecentPlay.ButtonText = "Recent Plays";
            this.btnRecentPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecentPlay.DisabledColor = System.Drawing.Color.Gray;
            this.btnRecentPlay.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRecentPlay.Iconimage = global::Music_app.Properties.Resources.Clock_32px;
            this.btnRecentPlay.Iconimage_right = null;
            this.btnRecentPlay.Iconimage_right_Selected = null;
            this.btnRecentPlay.Iconimage_Selected = null;
            this.btnRecentPlay.IconMarginLeft = -5;
            this.btnRecentPlay.IconMarginRight = 0;
            this.btnRecentPlay.IconRightVisible = true;
            this.btnRecentPlay.IconRightZoom = 0D;
            this.btnRecentPlay.IconVisible = true;
            this.btnRecentPlay.IconZoom = 55D;
            this.btnRecentPlay.IsTab = false;
            this.btnRecentPlay.Location = new System.Drawing.Point(3, 141);
            this.btnRecentPlay.Name = "btnRecentPlay";
            this.btnRecentPlay.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnRecentPlay.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btnRecentPlay.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRecentPlay.selected = false;
            this.btnRecentPlay.Size = new System.Drawing.Size(197, 48);
            this.btnRecentPlay.TabIndex = 1;
            this.btnRecentPlay.Text = "Recent Plays";
            this.btnRecentPlay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecentPlay.Textcolor = System.Drawing.Color.White;
            this.btnRecentPlay.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecentPlay.Click += new System.EventHandler(this.btnRecentPlay_Click);
            // 
            // btn_MyMusic
            // 
            this.btn_MyMusic.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btn_MyMusic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btn_MyMusic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_MyMusic.BorderRadius = 0;
            this.btn_MyMusic.ButtonText = "My Music";
            this.btn_MyMusic.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_MyMusic.DisabledColor = System.Drawing.Color.Gray;
            this.btn_MyMusic.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_MyMusic.Iconimage = global::Music_app.Properties.Resources.Music_32px;
            this.btn_MyMusic.Iconimage_right = null;
            this.btn_MyMusic.Iconimage_right_Selected = null;
            this.btn_MyMusic.Iconimage_Selected = null;
            this.btn_MyMusic.IconMarginLeft = -5;
            this.btn_MyMusic.IconMarginRight = 0;
            this.btn_MyMusic.IconRightVisible = true;
            this.btn_MyMusic.IconRightZoom = 0D;
            this.btn_MyMusic.IconVisible = true;
            this.btn_MyMusic.IconZoom = 55D;
            this.btn_MyMusic.IsTab = false;
            this.btn_MyMusic.Location = new System.Drawing.Point(3, 87);
            this.btn_MyMusic.Name = "btn_MyMusic";
            this.btn_MyMusic.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btn_MyMusic.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(106)))), ((int)(((byte)(185)))));
            this.btn_MyMusic.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_MyMusic.selected = true;
            this.btn_MyMusic.Size = new System.Drawing.Size(197, 48);
            this.btn_MyMusic.TabIndex = 1;
            this.btn_MyMusic.Text = "My Music";
            this.btn_MyMusic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_MyMusic.Textcolor = System.Drawing.Color.White;
            this.btn_MyMusic.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MyMusic.Click += new System.EventHandler(this.btn_MyMusic_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbLogo);
            this.panel1.Controls.Add(this.btnMenu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 82);
            this.panel1.TabIndex = 0;
            // 
            // lbLogo
            // 
            this.lbLogo.AutoSize = true;
            this.lbLogo.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLogo.ForeColor = System.Drawing.Color.White;
            this.lbLogo.Location = new System.Drawing.Point(13, 18);
            this.lbLogo.Name = "lbLogo";
            this.lbLogo.Size = new System.Drawing.Size(141, 33);
            this.lbLogo.TabIndex = 1;
            this.lbLogo.Text = "Music Player";
            // 
            // btnMenu
            // 
            this.btnMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.btnMenu.Image = global::Music_app.Properties.Resources.Menu_48px;
            this.btnMenu.ImageActive = null;
            this.btnMenu.Location = new System.Drawing.Point(165, 6);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(29, 26);
            this.btnMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMenu.TabIndex = 0;
            this.btnMenu.TabStop = false;
            this.btnMenu.Zoom = 10;
            this.btnMenu.Click += new System.EventHandler(this.bunifuImageButton4_Click);
            // 
            // panel_Control
            // 
            this.panel_Control.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.panel_Control.Controls.Add(this.main_panel);
            this.panel_Control.Controls.Add(this.Music_control);
            this.panel_Control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Control.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel_Control.Location = new System.Drawing.Point(203, 41);
            this.panel_Control.Name = "panel_Control";
            this.panel_Control.Size = new System.Drawing.Size(597, 445);
            this.panel_Control.TabIndex = 2;
            // 
            // main_panel
            // 
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.Location = new System.Drawing.Point(0, 0);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(597, 350);
            this.main_panel.TabIndex = 1;
            // 
            // Music_control
            // 
            this.Music_control.Controls.Add(this.totalTime);
            this.Music_control.Controls.Add(this.currentTime);
            this.Music_control.Controls.Add(this.timeSlide);
            this.Music_control.Controls.Add(this.slVolume);
            this.Music_control.Controls.Add(this.btnNext);
            this.Music_control.Controls.Add(this.btnVolume);
            this.Music_control.Controls.Add(this.btnShuff);
            this.Music_control.Controls.Add(this.btnRepeat);
            this.Music_control.Controls.Add(this.btnPrev);
            this.Music_control.Controls.Add(this.btnPlay);
            this.Music_control.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Music_control.Location = new System.Drawing.Point(0, 350);
            this.Music_control.Name = "Music_control";
            this.Music_control.Size = new System.Drawing.Size(597, 95);
            this.Music_control.TabIndex = 0;
            // 
            // totalTime
            // 
            this.totalTime.AutoSize = true;
            this.totalTime.ForeColor = System.Drawing.Color.White;
            this.totalTime.Location = new System.Drawing.Point(543, 13);
            this.totalTime.Name = "totalTime";
            this.totalTime.Size = new System.Drawing.Size(34, 13);
            this.totalTime.TabIndex = 3;
            this.totalTime.Text = "00:00";
            this.totalTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentTime
            // 
            this.currentTime.AutoSize = true;
            this.currentTime.ForeColor = System.Drawing.Color.White;
            this.currentTime.Location = new System.Drawing.Point(12, 13);
            this.currentTime.Name = "currentTime";
            this.currentTime.Size = new System.Drawing.Size(34, 13);
            this.currentTime.TabIndex = 3;
            this.currentTime.Text = "00:00";
            this.currentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timeSlide
            // 
            this.timeSlide.BackColor = System.Drawing.Color.Transparent;
            this.timeSlide.BackgroudColor = System.Drawing.Color.DarkGray;
            this.timeSlide.BorderRadius = 0;
            this.timeSlide.Enabled = false;
            this.timeSlide.IndicatorColor = System.Drawing.Color.SeaGreen;
            this.timeSlide.Location = new System.Drawing.Point(64, 6);
            this.timeSlide.MaximumValue = 100;
            this.timeSlide.Name = "timeSlide";
            this.timeSlide.Size = new System.Drawing.Size(473, 30);
            this.timeSlide.TabIndex = 2;
            this.timeSlide.Value = 0;
            // 
            // slVolume
            // 
            this.slVolume.BackColor = System.Drawing.Color.Transparent;
            this.slVolume.BackgroudColor = System.Drawing.Color.DarkGray;
            this.slVolume.BorderRadius = 0;
            this.slVolume.IndicatorColor = System.Drawing.Color.SeaGreen;
            this.slVolume.Location = new System.Drawing.Point(487, 52);
            this.slVolume.MaximumValue = 100;
            this.slVolume.Name = "slVolume";
            this.slVolume.Size = new System.Drawing.Size(96, 30);
            this.slVolume.TabIndex = 1;
            this.slVolume.Value = 50;
            this.slVolume.ValueChanged += new System.EventHandler(this.slVolume_ValueChanged);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnNext.Image = global::Music_app.Properties.Resources.End_32px;
            this.btnNext.ImageActive = null;
            this.btnNext.Location = new System.Drawing.Point(279, 48);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(33, 34);
            this.btnNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnNext.TabIndex = 0;
            this.btnNext.TabStop = false;
            this.btnNext.Zoom = 10;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnVolume
            // 
            this.btnVolume.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnVolume.Image = global::Music_app.Properties.Resources.Speaker_32px;
            this.btnVolume.ImageActive = null;
            this.btnVolume.Location = new System.Drawing.Point(448, 48);
            this.btnVolume.Name = "btnVolume";
            this.btnVolume.Size = new System.Drawing.Size(33, 34);
            this.btnVolume.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnVolume.TabIndex = 0;
            this.btnVolume.TabStop = false;
            this.btnVolume.Zoom = 10;
            this.btnVolume.Click += new System.EventHandler(this.btnVolume_Click);
            // 
            // btnShuff
            // 
            this.btnShuff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnShuff.Image = global::Music_app.Properties.Resources.Shuffle_32px;
            this.btnShuff.ImageActive = null;
            this.btnShuff.Location = new System.Drawing.Point(360, 48);
            this.btnShuff.Name = "btnShuff";
            this.btnShuff.Size = new System.Drawing.Size(33, 34);
            this.btnShuff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnShuff.TabIndex = 0;
            this.btnShuff.TabStop = false;
            this.btnShuff.Zoom = 10;
            this.btnShuff.Click += new System.EventHandler(this.btnShuff_Click);
            // 
            // btnRepeat
            // 
            this.btnRepeat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnRepeat.Image = global::Music_app.Properties.Resources.Reset_32px;
            this.btnRepeat.ImageActive = null;
            this.btnRepeat.Location = new System.Drawing.Point(107, 48);
            this.btnRepeat.Name = "btnRepeat";
            this.btnRepeat.Size = new System.Drawing.Size(33, 34);
            this.btnRepeat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRepeat.TabIndex = 0;
            this.btnRepeat.TabStop = false;
            this.btnRepeat.Zoom = 10;
            this.btnRepeat.Click += new System.EventHandler(this.btnRepeat_Click);
            // 
            // btnPrev
            // 
            this.btnPrev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnPrev.Image = global::Music_app.Properties.Resources.Skip_to_Start_32px;
            this.btnPrev.ImageActive = null;
            this.btnPrev.Location = new System.Drawing.Point(182, 48);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(33, 34);
            this.btnPrev.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPrev.TabIndex = 0;
            this.btnPrev.TabStop = false;
            this.btnPrev.Zoom = 10;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(47)))), ((int)(((byte)(60)))));
            this.btnPlay.Image = global::Music_app.Properties.Resources.Play_32px;
            this.btnPlay.ImageActive = null;
            this.btnPlay.Location = new System.Drawing.Point(221, 42);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(52, 47);
            this.btnPlay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPlay.TabIndex = 0;
            this.btnPlay.TabStop = false;
            this.btnPlay.Zoom = 10;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.top_panel;
            this.bunifuDragControl1.Vertical = true;
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 10;
            this.bunifuElipse1.TargetControl = this;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            // 
            // bunifuDragControl2
            // 
            this.bunifuDragControl2.Fixed = true;
            this.bunifuDragControl2.Horizontal = true;
            this.bunifuDragControl2.TargetControl = this.songTitle;
            this.bunifuDragControl2.Vertical = true;
            // 
            // bunifuElipse2
            // 
            this.bunifuElipse2.ElipseRadius = 55;
            this.bunifuElipse2.TargetControl = this.btnRepeat;
            // 
            // bunifuElipse3
            // 
            this.bunifuElipse3.ElipseRadius = 50;
            this.bunifuElipse3.TargetControl = this.btnShuff;
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 486);
            this.Controls.Add(this.panel_Control);
            this.Controls.Add(this.menu_panel);
            this.Controls.Add(this.top_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Form_Load);
            this.top_panel.ResumeLayout(false);
            this.top_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_mini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_Close)).EndInit();
            this.menu_panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).EndInit();
            this.panel_Control.ResumeLayout(false);
            this.Music_control.ResumeLayout(false);
            this.Music_control.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnShuff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRepeat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel top_panel;
        private System.Windows.Forms.Panel menu_panel;
        private System.Windows.Forms.Panel panel_Control;
        private Bunifu.Framework.UI.BunifuImageButton btn_Close;
        private Bunifu.Framework.UI.BunifuImageButton btn_mini;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuImageButton btnMenu;
        private Bunifu.Framework.UI.BunifuFlatButton btn_MyMusic;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbLogo;
        private Bunifu.Framework.UI.BunifuFlatButton btnSetting;
        private Bunifu.Framework.UI.BunifuFlatButton btnPlaylist;
        private Bunifu.Framework.UI.BunifuFlatButton btnNowPlaying;
        private Bunifu.Framework.UI.BunifuFlatButton btnRecentPlay;
        private System.Windows.Forms.Panel main_panel;
        private System.Windows.Forms.Panel Music_control;
        private Bunifu.Framework.UI.BunifuImageButton btnPlay;
        private Bunifu.Framework.UI.BunifuSlider slVolume;
        private Bunifu.Framework.UI.BunifuImageButton btnNext;
        private Bunifu.Framework.UI.BunifuImageButton btnVolume;
        private Bunifu.Framework.UI.BunifuImageButton btnShuff;
        private Bunifu.Framework.UI.BunifuImageButton btnRepeat;
        private Bunifu.Framework.UI.BunifuImageButton btnPrev;
        private System.Windows.Forms.Label songTitle;
        private System.Windows.Forms.Label totalTime;
        private System.Windows.Forms.Label currentTime;
        private Bunifu.Framework.UI.BunifuSlider timeSlide;
        private System.Windows.Forms.Timer timer1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl2;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse2;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse3;
    }
}

